
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Moon-Haa
 */
public class Pratica31 {
    private static Date inicio;
    private static String meuNome = "Adriano Augusto Silva";
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1994,Calendar.FEBRUARY,9);
    private static Date fim;
    
    public static void main(String[] args) {
        inicio = new Date();
        System.out.println(meuNome.toUpperCase());
        System.out.println(Character.toUpperCase(meuNome.charAt(16))+meuNome.substring(17).toLowerCase()+", "+Character.toUpperCase(meuNome.charAt(0))+". "+Character.toUpperCase(meuNome.charAt(8))+".");
       // System.out.println(dataNascimento.getTime());
        System.out.println( (GregorianCalendar.getInstance().getTime().getTime() - dataNascimento.getTime().getTime()) / (24 * 3600 * 1000) );
        fim = new Date();
        System.out.println(fim.getTime()-inicio.getTime()+"ms");
    }
}
